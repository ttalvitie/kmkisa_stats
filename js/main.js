
// Change this to contain all team names that you expect to get from DB
var teamArray = ['MMG Tampere', 'MMG Turku'];

$(document).ready(function()
{
    // Put team names to header
    $("#team-names").text(teamArray.join(" vs. "));
    
    // Create a list of objects where to store all teams' chart data
    chartStorage = [];
    
    for(var i =0; i < teamArray.length; i++)
    {
        // Create an object for each team that contains their name
        // and placeholder arrays for all chart data.
        
        var teamObj = { 'name' : teamArray[i],
            'charts': {
                'current_standing_chart': [],                
                'km_avg_chart': [],
                'km_total_chart': [],
                'days_avg_chart': [],
                'days_total_chart': [],
                'gas_spared_chart': [],
                'co2_spared_chart': [],                  
            }
        }
        chartStorage.push(teamObj);
    }


    // Retrieve all stored daily team data snapshots from sqlite
    $.ajax({
      type: "POST",
      url: "kmkisa/interface.py/fetch_daily_stats"
    }).done(function(jsonData) {
       
       // After fetching, start working with the data...
       var res = JSON.parse(jsonData);
       
       // Iterate through the JSON result
       for (var i= 0; i < res.length; i++)
       {
           var teamName = res[i].team;
           
           // For each row, check what team the row belongs to.
           // Push the row data to appropriate arrays from where
           // they will eventually added to flot charts
           
           for(var j = 0; j < chartStorage.length; j++)
           {
                // When a matching object is found from chartStorage
                // add all row data to that object.
                if(chartStorage[j].name == teamName)
                {
                    // Quick n dirty fix for aligning the timestamp properly with flot label
                    var flotTime = (res[i].timestamp+10800)*1000;
                    
                    // Push the timestamp+value pairs to each chart's own array
                    chartStorage[j]['charts']['current_standing_chart'].push([flotTime, res[i].current_standing]);
                    
                    chartStorage[j]['charts']['km_avg_chart'].push([flotTime, res[i].km_avg]);
                    chartStorage[j]['charts']['km_total_chart'].push([flotTime, res[i].km_total]);
                    
                    chartStorage[j]['charts']['days_avg_chart'].push([flotTime, res[i].days_avg]);
                    chartStorage[j]['charts']['days_total_chart'].push([flotTime, res[i].days_total]);
                    
                    chartStorage[j]['charts']['gas_spared_chart'].push([flotTime, res[i].gas_spared]);
                    chartStorage[j]['charts']['co2_spared_chart'].push([flotTime, res[i].co2_spared]);                                        
                }
           }
       }
       
       
       // After all sqlite rows have been iterated, combine chart data of the same type from each team
       // to separate lists that will then be given to flot for rendering.
       // Also add the team name to be used as a label.
       
       current_standing_arrays  = [];
       km_total_arrays          = [];
       km_avg_arrays            = [];
       days_total_arrays        = [];
       days_avg_arrays          = [];
       gas_spared_arrays        = [];
       co2_spared_arrays        = [];
       
       for(var i = 0; i < chartStorage.length; i++)
       {
            current_standing_arrays.push({ 
                'label': chartStorage[i]['name'],
                'data': chartStorage[i]['charts']['current_standing_chart']
            });
            
            km_total_arrays.push({ 
                'label': chartStorage[i]['name'],
                'data': chartStorage[i]['charts']['km_total_chart']
            });
            
            km_avg_arrays.push({ 
                'label': chartStorage[i]['name'],
                'data': chartStorage[i]['charts']['km_avg_chart']
            });
            days_total_arrays.push({ 
                'label': chartStorage[i]['name'],
                'data': chartStorage[i]['charts']['days_total_chart']
            });
            
            days_avg_arrays.push({ 
                'label': chartStorage[i]['name'],
                'data': chartStorage[i]['charts']['days_avg_chart']
            });
            
            gas_spared_arrays.push({ 
                'label': chartStorage[i]['name'],
                'data': chartStorage[i]['charts']['gas_spared_chart']
            });
            
            co2_spared_arrays.push({ 
                'label': chartStorage[i]['name'],
                'data': chartStorage[i]['charts']['co2_spared_chart']
            });
       }
       
       
       // Create a generic flot options object, suitable for all charts
       var opts = {
           colors: ['#BE0012','#230B87'],
           legend: {
                show: true,
                backgroundOpacity: 0.35,
            },
            series: {
				lines: {
					show: true
				},
				points: {
					show: true
                }
            },
			grid: {
				hoverable: true,
				clickable: true
			},                       
            xaxis: {
                mode: "time",
                max: (new Date("2014/05/31")).getTime(), // TODO: get min/max values dynamically e.g. based on current date
                timeformat: "%d.%m",
                tickSize: [1, "day"]
                
            }   
        };
        
        // Clone the basic options object and customize the current standings chart
        // -hardcoded minimum value
        // -inverted y-axis
        var csOpts = JSON.parse(JSON.stringify(opts))
        
        csOpts['yaxis'] = {
            min: 1,
            transform: function (v) { return -v; },  
            inverseTransform: function (v) { return -v; }  
        } 
        
       
        // Create the flot charts one by one
        $.plot("#current_standing_chart", current_standing_arrays, csOpts );
		$.plot("#km_total_chart", km_total_arrays, opts );
		$.plot("#km_avg_chart", km_avg_arrays, opts );
		$.plot("#days_total_chart", days_total_arrays, opts );
		$.plot("#days_avg_chart", days_avg_arrays, opts );
		$.plot("#gas_spared_chart", gas_spared_arrays, opts );
		$.plot("#co2_spared_chart", co2_spared_arrays, opts );
        
        
        // Create and style a placeholder for tooltip
        $("<div id='tooltip'></div>").css({
			position: "absolute",
			display: "none",
			border: "1px solid #fdd",
			padding: "2px",
			"background-color": "#fee",
			opacity: 0.80
		}).appendTo("body");
                
        
        // Bind hover functionality to all charts
        $(".demo-placeholder").bind("plothover", function (event, pos, item) {

            if (item) {
                // Format date as human-readable
                var x = item.datapoint[0];
                var xDate = new Date(x)
                var readable = xDate.getDate() + "." + (xDate.getMonth()+1) + "." + xDate.getFullYear();
                
                // The y-axis value
                var y = item.datapoint[1];

                // Concatenate label,date and y-axis value into tooltip
                $("#tooltip").html(item.series.label + " " + readable + ": <strong>" + y + "</strong>")
                    .css({top: item.pageY+5, left: item.pageX+5})
                    .fadeIn(200);
            } else {
                $("#tooltip").hide();
            }
		});        
    }); // end ajax
}); // end document.ready
