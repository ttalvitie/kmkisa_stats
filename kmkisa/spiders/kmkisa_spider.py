# -*- coding: utf-8 -*-
from scrapy.spider import Spider
from scrapy.selector import Selector

from kmkisa.sqlite_handler import sqlite_handler
from kmkisa.utils import get_value_from_crawl

class KmkisaSpider(Spider):
    name = "kmkisa"

    ''' Change this to contain the team pages you intend to crawl '''
    start_urls = [
        "https://www.kilometrikisa.fi/teams/mmg-tampere/",
        "https://www.kilometrikisa.fi/teams/mmg-turku/",
    ]

    def parse(self, response):
                
        sel = Selector(response)
    
        ''' Use xpath to pull all relevant info from the DOM that Scrapy loaded '''
        team                = sel.xpath('//div[contains(@class,"page-title")]//h1/text()')[0].extract() # Teamname
        current_standing    = sel.xpath('//ul[contains(@class,"team-contest-table")]/li[contains(@class,"data-item")]/strong/text()')[0].extract() # 745 (and some tab characters)
        km_avg              = sel.xpath('//ul[contains(@class,"team-contest-table")]/li[3]/text()')[0].extract() # 50,2 km/hlö
        km_total            = sel.xpath('//ul[contains(@class,"team-contest-table")]/li[4]/text()')[0].extract() # 351,3 km
        days_avg            = sel.xpath('//ul[contains(@class,"team-contest-table")]/li[5]/text()')[0].extract() # 5,29 pv/hlö
        days_total          = sel.xpath('//ul[contains(@class,"team-contest-table")]/li[6]/text()')[0].extract() # 37
        gas_spared          = sel.xpath('//ul[contains(@class,"team-contest-table")]/li[7]/text()')[0].extract() # 24 litraa
        co2_spared          = sel.xpath('//ul[contains(@class,"team-contest-table")]/li[8]/text()')[0].extract() # 60 kg

        ''' Prepare a database connection '''
        sh = sqlite_handler()
        
        ''' Save data to DB, but remove whitespaces/descriptions for all values first ''' 
        sh.save_daily_stats(get_value_from_crawl(team, split=False),
            get_value_from_crawl(current_standing),
            get_value_from_crawl(km_avg),
            get_value_from_crawl(km_total),
            get_value_from_crawl(days_avg),
            get_value_from_crawl(days_total),
            get_value_from_crawl(gas_spared),
            get_value_from_crawl(co2_spared))
