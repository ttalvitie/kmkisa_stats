# -*- coding: utf-8 -*-

def get_value_from_crawl(input,split=True):
    ''' Take an xpath crawl result and strip all whitespaces and replace commas with dots.
        split param indicates only returning characters up to the first space (used for all numeric values) '''
    
    if split:
        return input.strip().replace(',', '.').split(' ')[0]
    else:
        return input.strip().replace(',', '.')
