BEGIN TRANSACTION;
CREATE TABLE team_snapshot (id INTEGER PRIMARY KEY, days_avg NUMERIC, date TEXT, team TEXT, current_standing NUMERIC, km_avg NUMERIC, km_total NUMERIC, days_total NUMERIC, gas_spared NUMERIC, co2_spared NUMERIC);
COMMIT;
