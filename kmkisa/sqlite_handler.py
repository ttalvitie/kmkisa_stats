# -*- coding: utf-8 -*-
import sqlite3 as sqlite
import sys
import time
import json
import datetime

class sqlite_handler:

    ''' Change this to your DB location '''
    db_path = '/etc/sqlite/kmkisa.sqlite'
    
    
    def __init__(self):
        ''' Open a connection to the DB upon instantation'''
        try:
            print "trying to open " + self.db_path
            self.conn = sqlite.connect(self.db_path)
        except sqlite.Error, e:
            print "Error connecting to DB - %s:" % e.args[0]
        
        
    def fetch_daily_stats(self):

        cursor = self.conn.cursor()
        
        select = "SELECT id, date, team, current_standing, km_avg, km_total, days_avg, days_total, gas_spared, co2_spared from team_snapshot;"
        cursor.execute(select)

        snapshot_list = []
        
        for row in cursor.fetchall():
            dict = {
                'id': row[0],            
                'timestamp': time.mktime(datetime.datetime.strptime(row[1], '%Y-%m-%d').timetuple()), # yyyy-mm-dd to timestamp
                'team': row[2],
                'current_standing': row[3],
                'km_avg': row[4],
                'km_total': row[5],
                'days_avg': row[6],
                'days_total': row[7],
                'gas_spared': row[8],
                'co2_spared': row[9],                            
            }
            
            snapshot_list.append(dict)
            
        j = json.dumps(snapshot_list)
        return j
        
    def save_daily_stats(self, team_name, current_standing, km_avg, km_total, days_avg, days_total, gas_spared, co2_spared):
        ''' Insert a new row to the DB containing the current stats for one team. '''
        #now = time.time() # TODO!!!
        now = datetime.datetime.fromtimestamp(int(time.time())).strftime('%Y-%m-%d')
                
        try:
            cursor = self.conn.cursor()
            cursor.execute('INSERT INTO team_snapshot(date, team, current_standing, km_avg, km_total, days_avg, days_total, gas_spared, co2_spared) VALUES (?,?,?,?,?,?,?,?,?)', (now, team_name, current_standing, km_avg, km_total, days_avg, days_total, gas_spared, co2_spared))
            self.conn.commit()
            
        except sqlite.Error, e:
            
            print "Error %s:" % e.args[0]
            sys.exit(1)
            
        finally:
            if self.conn:
                self.conn.close()        
