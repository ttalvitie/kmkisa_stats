#!/usr/bin/env python

from sqlite_handler import sqlite_handler


def index(req):
    return str(req)

def fetch_daily_stats(req):
    sh = sqlite_handler()        
    return sh.fetch_daily_stats()
